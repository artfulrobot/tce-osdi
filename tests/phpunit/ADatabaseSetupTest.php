<?php

use Civi\Test\HeadlessInterface;
use Civi\Test\TransactionalInterface;

/**
 * @group headless
 */
class ADatabaseSetupTest extends \PHPUnit\Framework\TestCase implements
    HeadlessInterface,
    TransactionalInterface {

  public function setUpHeadless(): \Civi\Test\CiviEnvBuilder {
    return \Civi\Test::headless()
      ->install(['osdi-client'])
      ->installMe(__DIR__)
      ->apply();
  }

  public function testOsdiClientClassExists() {
    self::assertTrue(class_exists(\Civi\OsdiClient::class));
  }

}
