<?php

namespace Civi;

use Civi;

class TceOsdiTestUtil {

  public static function tellTheContainerToUseTCEClasses(): void {
    $container = Civi\OsdiClient::container();
    $container->register('BatchSyncer', 'Donation',
      Civi\Osdi\ActionNetwork\BatchSyncer\DonationLimitedToGroup::class);
    $container->register('BatchSyncer', 'Person',
      Civi\Osdi\ActionNetwork\BatchSyncer\PersonLimitedToGroup::class);
    $container->register('LocalObject', 'Donation',
      Civi\Osdi\LocalObject\DonationTce::class);
    $container->register('LocalObject', 'Person',
      Civi\Osdi\LocalObject\PersonTce::class);
    $container->register('Mapper', 'Donation',
      Civi\Osdi\ActionNetwork\Mapper\DonationTce::class);
    $container->register('Mapper', 'Person',
      Civi\Osdi\ActionNetwork\Mapper\PersonTce::class);
    $container->register('SingleSyncer', 'Donation',
      \Civi\Osdi\ActionNetwork\SingleSyncer\DonationTce::class);
    $container->register('SingleSyncer', 'Person',
      \Civi\Osdi\ActionNetwork\SingleSyncer\PersonTce::class);
  }

}
