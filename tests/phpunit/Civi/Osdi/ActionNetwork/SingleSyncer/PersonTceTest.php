<?php

namespace Civi\Osdi\ActionNetwork\SingleSyncer;

use Civi;
use Civi\Osdi\LocalObject\PersonTce as LocalPerson;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;
use OsdiClient\ActionNetwork\TestUtils;

/**
 * @group headless
 */
class PersonTceTest extends \PHPUnit\Framework\TestCase implements
    HeadlessInterface,
    HookInterface,
    TransactionalInterface {

  public static Civi\Osdi\Container $container;

  public static PersonTce $syncer;

  private static function setUpCustomConfig(): void {
    self::setUpSyncProfile();
    \CRM_Core_Config::singleton()->defaultContactCountry = 1228;
  }

  private static function setUpSyncProfile() {
    \Civi\Api4\OsdiSyncProfile::save(FALSE)
      ->setRecords([
        [
          'id' => TestUtils::createSyncProfile()['id'],
          'entry_point' => __CLASS__,
          'is_default' => TRUE,
          'classes' => [
            'LocalObject' => ['Person' => Civi\Osdi\LocalObject\PersonTce::class],
            'Mapper' => ['Person' => \Civi\Osdi\ActionNetwork\Mapper\PersonTce::class],
            'SingleSyncer' => ['Person' => \Civi\Osdi\ActionNetwork\SingleSyncer\PersonTce::class],
          ],
        ],
      ])->execute();

    self::$container = \Civi\OsdiClient::containerWithDefaultSyncProfile(TRUE);
  }

  public function setUpHeadless(): \Civi\Test\CiviEnvBuilder {
    return \Civi\Test::headless()
      ->install(['osdi-client'])
      ->installMe(__DIR__)
      ->apply();
  }

  public static function setUpBeforeClass(): void {
    parent::setUpBeforeClass();
    self::setUpCustomConfig();
    self::$syncer = self::$container->getSingle('SingleSyncer', 'Person');
  }

  /**
   * @return \Civi\Osdi\ActionNetwork\Object\Person
   * @throws \Civi\Osdi\Exception\InvalidArgumentException
   */

  private function  getCookieCutterCiviContact(): array {
    $createContact = Civi\Api4\Contact::create()->setValues(
      [
        'first_name' => 'Cookie',
        'last_name' => 'Cutter',
      ]
    )->addChain('email', \Civi\Api4\Email::create()
      ->setValues(
        [
          'contact_id' => '$id',
          'email' => 'cookie@yum.net',
        ]
      )
    )->addChain('phone', \Civi\Api4\Phone::create()
      ->setValues(
        [
          'contact_id' => '$id',
          'phone' => '12023334444',
          'phone_type_id:name' => 'Mobile',
        ]
      )
    )->addChain('address', \Civi\Api4\Address::create()
      ->setValues(
        [
          'contact_id' => '$id',
          'street_address' => '123 Test St',
          'city' => 'Licking',
          'state_province_id:name' => 'Missouri',
          'postal_code' => 65542,
          'country_id:name' => 'US',
        ]
      )
    )->execute();
    $cid = $createContact->single()['id'];
    return Civi\Api4\Contact::get(FALSE)
      ->addWhere('id', '=', $cid)
      ->addJoin('Address')->addJoin('Email')->addJoin('Phone')
      ->addSelect('*', 'address.*', 'address.state_province_id:name', 'address.country_id:name', 'email.*', 'phone.*')
      ->execute()
      ->single();
  }

  public function testPreviousSmsStatusIsSaved() {
    $civiContact = $this->getCookieCutterCiviContact();
    /** @var \Civi\Osdi\LocalObject\PersonTce $localPerson */
    $localPerson = LocalPerson::fromId($civiContact['id']);
    $localPerson->doNotSms->set(FALSE);
    $localPerson->preferredCommunicationMethod->set(['SMS']);
    $localPerson->save();


    // FIRST SYNC

    $pair = self::$syncer->matchAndSyncIfEligible($localPerson);

    /** @var \Civi\Osdi\ActionNetwork\Object\Person $remotePerson */
    $remotePerson = $pair->getRemoteObject();
    self::assertNotNull($remotePerson, print_r($pair->getResultStack()->toArray(), TRUE));
    self::assertEquals('subscribed', $remotePerson->phoneStatus->get());
    self::assertNotNull($remotePerson->getId());

    $savedStatus = \Civi\Api4\OsdiPersonOldState::get(FALSE)
      ->addWhere('remote_person_id', '=', $remotePerson->getId())
      ->execute();

    self::assertCount(1, $savedStatus);

    self::assertEquals(TRUE, $savedStatus->last()['sms_subscribed']);


    // SECOND SYNC - DIFFERENT SMS STATUS

    $localPerson->doNotSms->set(TRUE);
    $localPerson->preferredCommunicationMethod->set([]);
    $localPerson->save();

    TestUtils::makeItSeemLikePersonWasModifiedAfterLastSync($localPerson);

    $pair = self::$syncer->matchAndSyncIfEligible($localPerson);

    /** @var \Civi\Osdi\ActionNetwork\Object\Person $remotePerson */
    $remotePerson = $pair->getRemoteObject();
    self::assertEquals('unsubscribed', $remotePerson->phoneStatus->get(),
      print_r($pair->getResultStack()->toArray(), TRUE));
    self::assertNotNull($remotePerson->getId());

    $savedStatus = Civi\Api4\OsdiPersonOldState::get(FALSE)
      ->addWhere('remote_person_id', '=', $remotePerson->getId())
      ->execute();

    self::assertCount(1, $savedStatus);

    self::assertEquals(FALSE, $savedStatus->last()['sms_subscribed']);
  }

}
