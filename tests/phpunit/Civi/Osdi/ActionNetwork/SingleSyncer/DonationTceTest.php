<?php

namespace Civi\Osdi\ActionNetwork\SingleSyncer;

use Civi;
use Civi\Core\HookInterface;
use Civi\Osdi\Result\SyncEligibility;
use Civi\Test\HeadlessInterface;
use Civi\Test\TransactionalInterface;
use OsdiClient\ActionNetwork\TestUtils;

/**
 * Test \Civi\Osdi\RemoteSystemInterface
 *
 * @group headless
 */
class DonationTceTest extends \PHPUnit\Framework\TestCase implements
    HeadlessInterface,
    HookInterface,
    TransactionalInterface {

  use Civi\Osdi\ActionNetwork\DonationHelperTrait;

  private static DonationTce $syncer;

  private static Civi\Osdi\RemoteObjectInterface $testRemotePerson;

  private static Civi\Osdi\LocalObjectInterface $testLocalPerson;

  public function setUpHeadless(): \Civi\Test\CiviEnvBuilder {
    return \Civi\Test::headless()
      ->install('osdi-client')
      ->installMe(__DIR__)
      ->apply();
  }

  public static function setUpBeforeClass(): void {
    self::$system = TestUtils::createRemoteSystem();
    Civi\TceOsdiTestUtil::tellTheContainerToUseTCEClasses();
    $container = Civi\OsdiClient::container();
    self::$syncer = $container->getSingle('SingleSyncer', 'Donation');

    // Create twin remote & local Persons to use in our tests
    $remotePerson = $container->make('OsdiObject', 'osdi:people');
    $remotePerson->givenName->set('Wilma');
    $remotePerson->familyName->set('Flintstone');
    $remotePerson->emailAddress->set('wilma@example.org');
    $remotePerson->save();
    static::$testRemotePerson = $remotePerson;
    /** @var \Civi\Osdi\ActionNetwork\SingleSyncer\PersonTce $personSyncer */
    $personSyncer = $container->getSingle('SingleSyncer', 'Person');
    $pair = $personSyncer->matchAndSyncIfEligible($remotePerson);
    static::$testLocalPerson = $pair->getLocalObject();
    self::assertIsNumeric(static::$testLocalPerson->getId(),
      print_r($pair->getResultStack()->toArray(), TRUE));
    static::$financialTypeId = static::getTestFinancialTypeId();
    static::setLocalTimeZone('CDT');
  }

  public function testEligibility() {
    // Create fixtures
    $makeDonation = function (string $code) {
      $d = new Civi\Osdi\LocalObject\DonationTce();
      $d->currency->set('USD');
      $d->setPerson(static::$testLocalPerson);
      $d->amount->set(substr(microtime(TRUE) * 100, -9));
      $d->financialTypeId->set(static::$financialTypeId);
      $d->receiveDate->set(date('Y-m-d H:i:s'));
      $d->tceFundraisingCode->set([$code]);
      return $d->save();
    };

    $localDonationEligible = $makeDonation('FC');
    $localDonationIneligible = $makeDonation('R Bad');

    // Call system under test
    $pairFromEligible = static::$syncer->matchAndSyncIfEligible($localDonationEligible);
    $pairFromIneligible = static::$syncer->matchAndSyncIfEligible($localDonationIneligible);

    // Check expectations
    $eligibility1 = $pairFromEligible->getLastResultOfType(SyncEligibility::class);
    self::assertEquals(SyncEligibility::ELIGIBLE, $eligibility1->getStatusCode());

    $eligibility2 = $pairFromIneligible->getLastResultOfType(SyncEligibility::class);
    self::assertEquals(SyncEligibility::INELIGIBLE, $eligibility2->getStatusCode());
  }

  public function testMapLocalToNewRemote() {

    // Create fixture
    $financialTypeId = \Civi\Api4\FinancialType::save(FALSE)
      ->setRecords([[
        'name' => 'PAC - DCE',
        'description' => 'Used by PHPUnit test ' . __CLASS__ . '::' . __FUNCTION__,
      ]])
      ->setMatch(['name'])
      ->execute()->first()['id'];

    $localDonation = new Civi\Osdi\LocalObject\DonationTce();
    $localDonation->currency->set('USD');
    $localDonation->setPerson(static::$testLocalPerson);
    $localDonation->amount->set('3.33');
    $localDonation->financialTypeId->set($financialTypeId);

    $time = new \DateTime('-10 years');
    $localTimezone = new \DateTimeZone(\Civi::settings()->get('osdiClient.localUtcOffset'));
    $localTimeString = $time->setTimezone($localTimezone)->format('Y-m-d H:i:s');

    $localDonation->receiveDate->set($localTimeString);
    $localDonation->tceFundraisingCode->set([]);
    $localDonation->save();

    // Call system under test
    $remoteDonation = static::$syncer->matchAndSyncIfEligible($localDonation)
      ->getRemoteObject();

    // Check expectations
    $this->assertEquals(
      static::$testRemotePerson->getId(),
      $remoteDonation->getDonor()->getId());

    $this->assertEquals(
      $time->setTimezone(new \DateTimeZone('UTC'))->format('Y-m-d\TH:i:s\Z'),
      $remoteDonation->createdDate->get());

    $this->assertEquals(
      [['display_name' => 'PAC - DCE', 'amount' => '3.33']],
      $remoteDonation->recipients->get());

    $this->assertEquals('usd', $remoteDonation->currency->get());

    $this->assertEquals(
      ['recurring' => FALSE],
      $remoteDonation->recurrence->get());

    $this->assertEquals(
      'TCE PAC - DCE CiviCRM',
      $remoteDonation->getFundraisingPage()->title->get());
  }

}
