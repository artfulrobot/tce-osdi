<?php

namespace Civi\Osdi\ActionNetwork\Mapper;

use Civi;
use Civi\Core\HookInterface;
use Civi\Osdi\ActionNetwork\Mapper\DonationTce as DonationTceMapper;
use Civi\Osdi\ActionNetwork\Object\Donation as RemoteDonation;
use Civi\Osdi\ActionNetwork\Object\FundraisingPage;
use Civi\Test\HeadlessInterface;
use Civi\Test\TransactionalInterface;
use OsdiClient\ActionNetwork\TestUtils;

/**
 * Test \Civi\Osdi\RemoteSystemInterface
 *
 * @group headless
 */
class DonationTceTest extends \PHPUnit\Framework\TestCase implements
  HeadlessInterface,
  HookInterface,
  TransactionalInterface {

  /**
   * @var array{Contact: array, OptionGroup: array, OptionValue: array,
   *   CustomGroup: array, CustomField: array}
   */
  private static $createdEntities = [];

  private static Civi\Osdi\ActionNetwork\RemoteSystem $system;

  private static Civi\Osdi\ActionNetwork\Object\Person $testRemotePerson;

  private static Civi\Osdi\ActionNetwork\Matcher\Person\UniqueEmailOrFirstLastEmail $personMatcher;

  private static FundraisingPage $testFundraisingPage;

  private static int $financialTypeId;

  private static $testLocalPerson;

  public function setUpHeadless(): \Civi\Test\CiviEnvBuilder {
    return \Civi\Test::headless()
      ->install('osdi-client')
      ->installMe(__DIR__)
      ->apply();
  }

  public function setUp(): void {
    Civi\TceOsdiTestUtil::tellTheContainerToUseTCEClasses();
    $this->mapper = new DonationTceMapper(static::$system);
    parent::setUp();
  }

  public static function setUpBeforeClass(): void {
    static::$system = TestUtils::createRemoteSystem();
    TestUtils::createSyncProfile();

    // We need a remote person that matches a local person.
    // ... get the remote person
    $remotePerson = new Civi\Osdi\ActionNetwork\Object\Person(static::$system);
    $remotePerson->givenName->set('Wilma');
    $remotePerson->familyName->set('Flintstone');
    $remotePerson->emailAddress->set('wilma@example.org');
    $remotePerson->save();
    static::$testRemotePerson = $remotePerson;

    // ... use sync to get local person
    $personSyncer = new \Civi\Osdi\ActionNetwork\SingleSyncer\PersonBasic(static::$system);
    $pair = $personSyncer->matchAndSyncIfEligible($remotePerson);
    static::$testLocalPerson = $pair->getLocalObject();
    static::$createdEntities['Contact'] = [static::$testLocalPerson->getId()];
    // HACK: the above sometimes returns a deleted contact.
    $neededToUndelete = \Civi\Api4\Contact::update(FALSE)
      ->addWhere('id', '=', static::$testLocalPerson->getId())
      ->addValue('is_deleted', 0)
      ->addWhere('is_deleted', '=', 1)
      ->execute()
      ->count();

    // Ensure we have the default fundraising page.
    $fundraisingPages = static::$system->findAll('osdi:fundraising_pages');
    $found = FALSE;
    foreach ($fundraisingPages as $fundraisingPage) {
      if ($fundraisingPage->title->get() === DonationTceMapper::FUNDRAISING_PAGE_NAME) {
        $found = $fundraisingPage;
        break;
      }
    }
    if (!$found) {
      // Create the default fundraising page now.
      // @Todo expect this code to change.

      $fundraisingPage = new FundraisingPage(static::$system);
      $fundraisingPage->name->set(DonationTceMapper::FUNDRAISING_PAGE_NAME);
      // Nb. title is the public title and is required according to the API,
      // even though there should not be a public page for API-created
      // fundraising pages.
      $fundraisingPage->title->set(DonationTceMapper::FUNDRAISING_PAGE_NAME);
      $fundraisingPage->originSystem->set('CiviCRM');
      $fundraisingPage->save();
    }
    static::$testFundraisingPage = $fundraisingPage;

    static::$financialTypeId = \Civi\Api4\FinancialType::save(FALSE)
      ->setRecords([[
          'name' => 'TCE',
          'description' => 'Used by PHPUnit test ' . __CLASS__ . '::' . __FUNCTION__,
        ]])
      ->setMatch(['name'])
      ->execute()->first()['id'];
  }

  public function tearDown(): void {
    parent::tearDown();
  }

  public static function tearDownAfterClass(): void {

    static::$testRemotePerson->delete();

    foreach (self::$createdEntities as $type => $ids) {
      foreach ($ids as $id) {
        civicrm_api4($type, 'delete', [
          'where' => [['id', '=', $id]],
          'checkPermissions' => FALSE,
          'useTrash' => FALSE,
        ]);
      }
    }

    parent::tearDownAfterClass();
  }


  /**
   *
   * Remote → Local
   *
   */
  public function testMapRemoteToNewLocal() {

    // Create fixture
    $remoteDonation = new RemoteDonation(static::$system);
    $remoteDonation->currency->set('USD');
    $recipients = [['display_name' => 'Test recipient financial type', 'amount' => '2.22']];
    $remoteDonation->recipients->set($recipients);
    $remoteTimeString = '2020-03-04T05:06:07Z';
    $remoteDonation->createdDate->set($remoteTimeString);
    $remoteDonation->setDonor(self::$testRemotePerson);
    $remoteDonation->setFundraisingPage(self::$testFundraisingPage);
    $pageTitle = self::$testFundraisingPage->title->get();
    $this->assertNotEmpty($pageTitle);
    $remoteDonation->recurrence->set(['recurring' => FALSE]);
    $referrerData = [
      'source' => 'phpunit_source',
      'website' => 'https://example.org/test-referrer',
    ];
    $remoteDonation->referrerData->set($referrerData);
    $remoteDonation->save();

    $localTimezone = new \DateTimeZone(\Civi::settings()->get('osdiClient.localUtcOffset'));
    $time = new \DateTime($remoteTimeString);
    $localTimeString = $time->setTimezone($localTimezone)->format('Y-m-d H:i:s');

    // Call system under test
    $mapper = new DonationTceMapper(static::$system);
    /** @var \Civi\Osdi\LocalObject\DonationTce $localDonation */
    $localDonation = $mapper->mapRemoteToLocal($remoteDonation);
    $localDonation->save()->load();

    // Check expectations
    $this->assertEquals(static::$createdEntities['Contact'][0], $localDonation->getPerson()->getId());
    $this->assertEquals($localTimeString, $localDonation->receiveDate->get());
    $this->assertEquals('USD', $localDonation->currency->get());
    $this->assertEquals(static::$financialTypeId, $localDonation->financialTypeId->get());
    $this->assertNull($localDonation->contributionRecurId->get());

    // Check that the fundraising page name is found in the custom field as well as the source
    $this->assertEquals($pageTitle, $localDonation->tceFundraisingPage->get());
    $this->assertEquals("Action Network: $pageTitle", $localDonation->source->get());
  }

  /**
   * @dataProvider dataProviderMapLocalToNewRemote
   */
  public function testMapLocalToNewRemote(
    string $financialTypeLabel,
    array  $fundraisingCodes,
    string $expectedPageTitle,
    array  $expectedRecurrence
  ) {

    // Create fixture
    $financialTypeId = \Civi\Api4\FinancialType::save(FALSE)
      ->setRecords([[
        'name' => $financialTypeLabel,
        'description' => 'Used by PHPUnit test ' . __CLASS__ . '::' . __FUNCTION__,
      ]])
      ->setMatch(['name'])
      ->execute()->first()['id'];

    $localDonation = new Civi\Osdi\LocalObject\DonationTce();
    $localDonation->currency->set('USD');
    $localDonation->setPerson(static::$testLocalPerson);
    $localDonation->amount->set('3.33');
    $localDonation->financialTypeId->set($financialTypeId);
    $localTimeString = '2009-08-07 06:05:04';
    $localDonation->receiveDate->set($localTimeString);
    $localDonation->tceFundraisingCode->set($fundraisingCodes);
    $localDonation->save();

    // Call system under test
    $mapper = new DonationTceMapper();
    /** @var \Civi\Osdi\ActionNetwork\Object\Donation $remoteDonation */
    $remoteDonation = $mapper->mapLocalToRemote($localDonation);

    // Check expectations
    $this->assertEquals(
      static::$testRemotePerson->getId(),
      $remoteDonation->getDonor()->getId());

    $localZone = \Civi::settings()->get('osdiClient.localUtcOffset');
    $this->assertEquals(
      strtotime("$localTimeString $localZone"),
      strtotime($remoteDonation->createdDate->get()));

    $this->assertEquals(
      [['display_name' => $financialTypeLabel, 'amount' => '3.33']],
      $remoteDonation->recipients->get());

    $this->assertEquals('usd', $remoteDonation->currency->get());

    $this->assertEquals(
      $expectedRecurrence,
      $remoteDonation->recurrence->get());

    $this->assertEquals(
      $expectedPageTitle,
      $remoteDonation->getFundraisingPage()->title->get());
  }

  public function dataProviderMapLocalToNewRemote(): array {
    return [
      'financial type "TCE", recurring' => [
        'TCE',
        ['PF Austin', 'PC Quarterly'],
        'TCE CiviCRM',
        ['recurring' => TRUE, 'period' => 'Every 3 Months'],
      ],
      'financial type "PAC - Main", one-time' => [
        'PAC - Main',
        [],
        'TCE PAC - Main CiviCRM',
        ['recurring' => FALSE],
      ],
    ];
  }

}

