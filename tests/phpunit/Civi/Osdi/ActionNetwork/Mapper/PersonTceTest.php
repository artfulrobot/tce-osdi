<?php

namespace Civi\Osdi\ActionNetwork\Mapper;

use Civi;
use Civi\Osdi\LocalObject\PersonTce as LocalPerson;
use Civi\Test\HeadlessInterface;
use Civi\Test\HookInterface;
use Civi\Test\TransactionalInterface;

/**
 * @group headless
 */
class PersonTceTest extends \PHPUnit\Framework\TestCase implements
    HeadlessInterface,
    HookInterface,
    TransactionalInterface {

  public static Civi\Osdi\Container $container;

  public static PersonTce $mapper;

  private static function setUpCustomConfig(): void {
    self::setUpSyncProfile();
    \CRM_Core_Config::singleton()->defaultContactCountry = 1228;
  }

  private static function setUpSyncProfile() {
    \Civi\Api4\OsdiSyncProfile::save(FALSE)
      ->setMatch(['entry_point'])
      ->setRecords([
        [
          'entry_point' => __CLASS__,
          'is_default' => TRUE,
          'classes' => [
            'LocalObject' => ['Person' => Civi\Osdi\LocalObject\PersonTce::class],
            'Mapper' => ['Person' => \Civi\Osdi\ActionNetwork\Mapper\PersonTce::class],
          ],
        ],
      ])->execute();

    self::$container = \Civi\OsdiClient::containerWithDefaultSyncProfile(TRUE);
  }

  public function setUpHeadless(): \Civi\Test\CiviEnvBuilder {
    return \Civi\Test::headless()
      ->install(['osdi-client'])
      ->installMe(__DIR__)
      ->apply();
  }

  public static function setUpBeforeClass(): void {
    parent::setUpBeforeClass();
    self::setUpCustomConfig();
    self::$mapper = self::$container->getSingle('Mapper', 'Person');
  }

  /**
   * @return \Civi\Osdi\ActionNetwork\Object\Person
   * @throws \Civi\Osdi\Exception\InvalidArgumentException
   */
  private function getCookieCutterOsdiPerson(): \Civi\Osdi\ActionNetwork\Object\Person {
    /** @var \Civi\Osdi\ActionNetwork\Object\Person $person */
    $person = self::$container->make('OsdiObject', 'osdi:people');
    $person->givenName->set('Cookie');
    $person->familyName->set('Cutter');
    $person->emailAddress->set('cookie@yum.net');
    $person->phoneNumber->set('12023334444');
    $person->postalStreet->set('202 N Main St');
    $person->postalLocality->set('Licking');
    $person->postalRegion->set('MO');
    $person->postalCode->set('65542');
    $person->postalCountry->set('US');
    return $person;
  }

  private function  getCookieCutterCiviContact(): array {
    $createContact = Civi\Api4\Contact::create()->setValues(
      [
        'first_name' => 'Cookie',
        'last_name' => 'Cutter',
      ]
    )->addChain('email', \Civi\Api4\Email::create()
      ->setValues(
        [
          'contact_id' => '$id',
          'email' => 'cookie@yum.net',
        ]
      )
    )->addChain('phone', \Civi\Api4\Phone::create()
      ->setValues(
        [
          'contact_id' => '$id',
          'phone' => '12023334444',
          'phone_type_id:name' => 'Mobile',
        ]
      )
    )->addChain('address', \Civi\Api4\Address::create()
      ->setValues(
        [
          'contact_id' => '$id',
          'street_address' => '123 Test St',
          'city' => 'Licking',
          'state_province_id:name' => 'Missouri',
          'postal_code' => 65542,
          'country_id:name' => 'US',
        ]
      )
    )->execute();
    $cid = $createContact->single()['id'];
    return Civi\Api4\Contact::get(FALSE)
      ->addWhere('id', '=', $cid)
      ->addJoin('Address')->addJoin('Email')->addJoin('Phone')
      ->addSelect('*', 'address.*', 'address.state_province_id:name', 'address.country_id:name', 'email.*', 'phone.*')
      ->execute()
      ->single();
  }

  /**
   *
   * LOCAL ===> REMOTE
   *
   */

  /**
   * @return array
   *   Each row has the following elements, all booleans:
   *   - do_not_email
   *   - is_opt_out
   *   - on_hold
   *   - actnet_subscribed (Whether, based on the above values, the email
   *     address should be marked "subscribed" in Action Network)
   */
  public function dataProviderMapLocalToNewRemoteWithEmailScenarios(): array {
    foreach (['', 'on_hold'] as $on_hold) {
      foreach (['', 'is_opt_out'] as $is_opt_out) {
        foreach (['', 'do_not_email'] as $do_not_email) {
          $actnet_subscribed = !($do_not_email || $is_opt_out || $on_hold);
          $key = implode(',', array_filter([$do_not_email, $is_opt_out, $on_hold]));
          $scenario = array_map(fn($s) => !!strlen($s), [$do_not_email, $is_opt_out, $on_hold]);
          $scenarios[$key] = [...$scenario, $actnet_subscribed];
          $order[] = array_sum($scenario) . $key;
        }
      }
    }
    array_multisort($order, $scenarios);
    return $scenarios;
  }

  /**
   * @param bool $do_not_email
   * @param bool $is_opt_out
   * @param bool $on_hold
   * @param bool $actnet_subscribed
   *
   * @dataProvider dataProviderMapLocalToNewRemoteWithEmailScenarios
   */
  public function testMapLocalToNewRemoteWithEmailScenarios(
    bool $do_not_email,
    bool $is_opt_out,
    bool $on_hold,
    bool $actnet_subscribed
  ) {
    $civiContact = $this->getCookieCutterCiviContact();
    $localPerson = LocalPerson::fromId($civiContact['id']);
    $localPerson->doNotEmail->set($do_not_email);
    $localPerson->isOptOut->set($is_opt_out);
    $localPerson->emailOnHold->set($on_hold ? 'On Hold Bounce' : 'No');
    $localPerson->save();

    /** @var \Civi\Osdi\ActionNetwork\Object\Person $result */
    $result = self::$mapper->mapLocalToRemote($localPerson);

    $remoteCustomFields = $result->customFields->get();

    self::assertEquals($civiContact['id'], $remoteCustomFields['CiviCRM Contact ID'] ?? '',
      print_r($result->getAll(), TRUE));

    self::assertEquals('CiviCRM', $remoteCustomFields['Contact Source'] ?? '',
      print_r($result->getAll(), TRUE));

    self::assertEquals($actnet_subscribed ? 'subscribed' : 'unsubscribed', $result->emailStatus->get());
  }

  /**
   * @return array{
   *           scenario: array{
   *             preferText: bool,
   *             doNotSms: bool,
   *             optOut: bool,
   *             mobilePhone: ?int,
   *             phonePhone: ?int,
   *             actNetPhone: string,
   *             actNetSmsStatus: string
   *             }
   *          }
   *
   * Scenarios:
   * - (1) Existing, new or updated records in Civi that include a mobile phone number are not assumed to be “subscribed” to receive AN SMS messages.
   * - (2,3) Existing, new or updated records in Civi that are marked as “Preferred Communications Method: Text message” are assumed to be “subscribed” to receive AN SMS messages. If the record in Civi includes a mobile phone number, that is the phone number synced into AN; if not, the primary phone number is synced into AN.
   * - (4) Existing, new or updated records in Civi that are marked as “Do Not SMS” are not “subscribed” to receive SMS messages in AN. If there is already a record in AN with the same phone number and it was previously “subscribed” to received SMS messages, it will be “unsubscribed.”
   * - (5) Records in Civi that are marked “User Opt Out” will be “unsubscribed” in AN
   * - (4) Records in Civi that are marked “Do not SMS” and “Preferred Communications Method: Text message” will be “unsubscribed” in AN
   */
  public function dataProviderMapLocalToNewRemoteWithSmsScenarios(): array {
    $keys = [ 'preferredCom',   'doNotSms', 'optOut', 'mobilePhone', 'phonePhone', 'actNetPhone',  'actNetSmsStatus'];
    $vals = [
      '#1' => [['Email'],        FALSE,      FALSE,    5551112222,    NULL,         5551112222,    'unsubscribed'],
      '#2' => [['SMS'],          FALSE,      FALSE,    5551112222,    6667778888,   5551112222,    'subscribed'  ],
      '#3' => [['SMS'],          FALSE,      FALSE,    NULL,          6667778888,   6667778888,    'subscribed'  ],
      '#4' => [['SMS'],          TRUE,       FALSE,    5551112222,    NULL,         5551112222,    'unsubscribed'],
      '#5' => [['SMS'],          FALSE,      TRUE,     5551112222,    NULL,         5551112222,    'unsubscribed'],
    ];
    foreach ($vals as $i => $scenario) {
      $return[$i]['scenario'] = array_combine($keys, $scenario);
    }
    return $return;
  }

  /**
   * @param array{
   *          preferredCom: ?string,
   *          doNotSms: bool,
   *          optOut: bool,
   *          mobilePhone: ?int,
   *          phonePhone: ?int,
   *          actNetPhone: string,
   *          actNetSmsStatus: string
   *          } $scenario
   *
   * @dataProvider dataProviderMapLocalToNewRemoteWithSmsScenarios
   */
  public function testMapLocalToNewRemoteWithSmsScenarios(array $scenario) {
    $civiContact = $this->getCookieCutterCiviContact();
    /** @var \Civi\Osdi\LocalObject\PersonTce $localPerson */
    $localPerson = LocalPerson::fromId($civiContact['id']);
    $localPerson->preferredCommunicationMethod->set($scenario['preferredCom']);
    $localPerson->doNotSms->set($scenario['doNotSms']);
    $localPerson->isOptOut->set($scenario['optOut']);
    $localPerson->mobilePhoneNumber->set($scenario['mobilePhone']);
    $localPerson->nonMobilePhoneNumber->set($scenario['phonePhone']);
    $localPerson->save();

    /** @var \Civi\Osdi\ActionNetwork\Object\Person $result */
    $result = self::$mapper->mapLocalToRemote($localPerson);

    self::assertEquals($scenario['actNetPhone'], $result->phoneNumber->get());
    self::assertEquals($scenario['actNetSmsStatus'], $result->phoneStatus->get());
  }

  public function testMapLocalToExistingRemote() {
    $civiContact = $this->getCookieCutterCiviContact();

    /** @var \Civi\Osdi\ActionNetwork\Object\Person $remotePersonWithId */
    $remotePersonWithId = self::$container->make('OsdiObject', 'osdi:people');
    $remotePersonWithId->setId('foooooo');

    $result = self::$mapper->mapLocalToRemote(
      new LocalPerson($civiContact['id']), $remotePersonWithId);

    $remoteCustomFields = $result->customFields->get();

    self::assertEquals($civiContact['id'], $remoteCustomFields['CiviCRM Contact ID'] ?? '',
      print_r($result->getAllWithoutLoading(), TRUE));

    self::assertNull($remoteCustomFields['Contact Source'] ?? NULL,
      print_r($result->getAllWithoutLoading(), TRUE));
  }

  /**
   *
   * REMOTE ===> LOCAL
   *
   */
  public function testRemoteToNewLocal() {
    $remotePerson = $this->getCookieCutterOsdiPerson();

    /** @var \Civi\Osdi\LocalObject\PersonTce $result */
    $result = self::$mapper->mapRemoteToLocal($remotePerson);

    self::assertEquals(LocalPerson::class, get_class($result));
    self::assertEquals('Action Network', $result->source->get());
  }

  public function testRemoteToExistingLocal() {
    $remotePerson = $this->getCookieCutterOsdiPerson();

    /** @var \Civi\Osdi\LocalObject\PersonTce $localPersonWithId */
    $localPersonWithId = self::$container->make('LocalObject', 'Person');
    $localPersonWithId->setId('1');

    /** @var \Civi\Osdi\LocalObject\PersonTce $result */
    $result = self::$mapper->mapRemoteToLocal($remotePerson, $localPersonWithId);

    self::assertEquals(LocalPerson::class, get_class($result));
    self::assertNull($result->source->get());
  }

  public function dataProviderMapRemoteToExistingLocalWithEmailScenarios(): array {
    $sets = [
      'stay subscribed' => [
        'AN status' => 'subscribed',
        'start: Civi do_not_email' => FALSE,
        'start: Civi is_opt_out' => FALSE,
        'start: Civi on_hold' => 'No',
        'start: Civi reason' => '',
        'end: Civi do_not_email' => FALSE,
        'end: Civi is_opt_out' => FALSE,
        'end: Civi on_hold' => 'No',
        'end: Civi reason' => '',
      ],
      'change to subscribed' => [
        'AN status' => 'subscribed',
        'start: Civi do_not_email' => TRUE,
        'start: Civi is_opt_out' => TRUE,
        'start: Civi on_hold' => 'On Hold Opt Out',
        'start: Civi reason' => 'something else',
        'end: Civi do_not_email' => FALSE,
        'end: Civi is_opt_out' => FALSE,
        'end: Civi on_hold' => 'No',
        'end: Civi reason' => '',
      ],
      'stay unsubscribed' => [
        'AN status' => 'unsubscribed',
        'start: Civi do_not_email' => TRUE,
        'start: Civi is_opt_out' => TRUE,
        'start: Civi on_hold' => 'On Hold Bounce',
        'start: Civi reason' => 'something else',
        'end: Civi do_not_email' => TRUE,
        'end: Civi is_opt_out' => TRUE,
        'end: Civi on_hold' => 'On Hold Bounce',
        'end: Civi reason' => 'something else',
      ],
      'change to unsubscribed' => [
        'AN status' => 'unsubscribed',
        'start: Civi do_not_email' => FALSE,
        'start: Civi is_opt_out' => FALSE,
        'start: Civi on_hold' => 'No',
        'start: Civi reason' => '',
        'end: Civi do_not_email' => TRUE,
        'end: Civi is_opt_out' => TRUE,
        'end: Civi on_hold' => 'No',
        'end: Civi reason' => 'Action Network Unsubscribe',
      ],
    ];
    return array_map(fn($a) => [$a], $sets);
  }

  /**
   * @dataProvider dataProviderMapRemoteToExistingLocalWithEmailScenarios
   */
  public function testMapRemoteToExistingLocalWithEmailScenarios(array $scenario) {
    $remotePerson = $this->getCookieCutterOsdiPerson();
    $remotePerson->emailStatus->set($scenario['AN status']);

    /** @var \Civi\Osdi\LocalObject\PersonTce $localPersonWithId */
    $localPersonWithId = self::$container->make('LocalObject', 'Person');
    $localPersonWithId->emailEmail->set('conductor@tra.in');
    $localPersonWithId->doNotEmail->set($scenario['start: Civi do_not_email']);
    $localPersonWithId->isOptOut->set($scenario['start: Civi is_opt_out']);
    $localPersonWithId->emailOnHold->set($scenario['start: Civi on_hold']);
    $localPersonWithId->dontEmailReason->set($scenario['start: Civi reason']);
    $localPersonWithId->save();

    /** @var \Civi\Osdi\LocalObject\PersonTce $result */
    $result = self::$mapper->mapRemoteToLocal($remotePerson, $localPersonWithId);
    $scenarioAsText = var_export($scenario, TRUE);

    self::assertEquals(LocalPerson::class, get_class($result));
    self::assertNull($result->source->get());

    self::assertEquals($scenario['end: Civi do_not_email'],
      $result->doNotEmail->get(), $scenarioAsText);
    self::assertEquals($scenario['end: Civi is_opt_out'],
      $result->isOptOut->get(), $scenarioAsText);
    self::assertEquals($scenario['end: Civi on_hold'],
      $result->emailOnHold->get(), $scenarioAsText);
    self::assertEquals($scenario['end: Civi reason'],
      $result->dontEmailReason->get(), $scenarioAsText);
  }

  public function testMobileNumberMismatch() {
    self::markTestIncomplete();
  }

  function dataProviderMapRemoteToLocalWithSmsScenarios() {
    $sets = [
      'new, subscribed, no number in Civi' => [
        'old AN SMS subscribed' => NULL,
        'new AN SMS status' => 'subscribed',
        'start: Civi mobile number' => NULL,
        'start: Preferred' => ['Email'],
        'start: do_not_sms' => FALSE,
        'end: Civi mobile number' => '12023334444',
        'end: Preferred' => ['Email', 'SMS'],
        'end: do_not_sms' => FALSE,
      ],
      'new, subscribed, matching number in Civi' => [
        'old AN SMS subscribed' => NULL,
        'new AN SMS status' => 'subscribed',
        'start: Civi mobile number' => '12023334444',
        'start: Preferred' => ['Email'],
        'start: do_not_sms' => TRUE,
        'end: Civi mobile number' => '12023334444',
        'end: Preferred' => ['Email', 'SMS'],
        'end: do_not_sms' => FALSE,
      ],
      'new, not subscribed, no number in Civi' => [
        'old AN SMS subscribed' => NULL,
        'new AN SMS status' => 'unsubscribed',
        'start: Civi mobile number' => NULL,
        'start: Preferred' => ['Email'],
        'start: do_not_sms' => FALSE,
        'end: Civi mobile number' => '12023334444',
        'end: Preferred' => ['Email'],
        'end: do_not_sms' => FALSE,
      ],
      'new, not subscribed, matching number in Civi' => [
        'old AN SMS subscribed' => NULL,
        'new AN SMS status' => 'unsubscribed',
        'start: Civi mobile number' => '12023334444',
        'start: Preferred' => ['Email', 'SMS'],
        'start: do_not_sms' => FALSE,
        'end: Civi mobile number' => '12023334444',
        'end: Preferred' => ['Email', 'SMS'],
        'end: do_not_sms' => FALSE,
      ],
      'unsubscribe, matching number in Civi' => [
        'old AN SMS subscribed' => TRUE,
        'new AN SMS status' => 'unsubscribed',
        'start: Civi mobile number' => '12023334444',
        'start: Preferred' => ['Email', 'SMS'],
        'start: do_not_sms' => FALSE,
        'end: Civi mobile number' => '12023334444',
        'end: Preferred' => ['Email'],
        'end: do_not_sms' => TRUE,
      ],
    ];
    return array_map(fn($a) => [$a], $sets);
  }

  /**
   * @dataProvider dataProviderMapRemoteToLocalWithSmsScenarios
   *
   * @param array{"old AN SMS subscribed": ?bool,"new AN SMS status": string,"start: Civi mobile number": ?string,"start: Preferred": array,"start: do_not_sms": bool,"end: Civi mobile number": ?string,"end: Preferred": array,"end: do_not_sms": bool} $scenario
   */
  function testMapRemoteToLocalWithSmsScenarios(array $scenario
  ) {
    $remotePerson = $this->getCookieCutterOsdiPerson();
    $remotePerson->setId('foobar');
    Civi\Api4\OsdiPersonOldState::save(FALSE)
      ->setMatch(['remote_person_id'])
      ->setRecords([[
        'remote_person_id' => 'foobar',
        'sms_subscribed' => $scenario['old AN SMS subscribed'],
        ]])
      ->execute();
    $remotePerson->phoneStatus->set($scenario['new AN SMS status']);

    /** @var \Civi\Osdi\LocalObject\PersonTce $localPersonWithId */
    $localPersonWithId = self::$container->make('LocalObject', 'Person');
    $localPersonWithId->mobilePhoneNumber->set($scenario['start: Civi mobile number']);
    $localPersonWithId->preferredCommunicationMethod->set($scenario['start: Preferred']);
    $localPersonWithId->doNotSms->set($scenario['start: do_not_sms']);
    $localPersonWithId->save();

    /** @var \Civi\Osdi\LocalObject\PersonTce $result */
    $result = self::$mapper->mapRemoteToLocal($remotePerson, $localPersonWithId);
    $scenarioAsText = var_export($scenario, TRUE);

    self::assertEquals(LocalPerson::class, get_class($result));

    self::assertEquals($scenario['end: Civi mobile number'],
      $result->mobilePhoneNumber->get(), $scenarioAsText);
    self::assertEquals($scenario['end: Preferred'],
      $result->preferredCommunicationMethod->get(), $scenarioAsText);
    self::assertEquals($scenario['end: do_not_sms'],
      $result->doNotSms->get(), $scenarioAsText);
  }

}
