<?php

namespace Civi\Osdi\ActionNetwork\BatchSyncer;

use Civi\Api4\OsdiPersonSyncState;
use Civi\Osdi\ActionNetwork\DonationHelperTrait;
use Civi\OsdiClient;
use PHPUnit;

/**
 * Test \Civi\Osdi\RemoteSystemInterface
 *
 * @group headless
 *
 * osdi-client classes should be autoloaded --
 * see code in <extension-root>/tests/phpunit/bootstrap.php
 */
class DonationLimitedToGroupTest extends PHPUnit\Framework\TestCase implements
    \Civi\Test\HeadlessInterface,
    \Civi\Test\TransactionalInterface {

  use DonationHelperTrait;

  public function setUpHeadless(): \Civi\Test\CiviEnvBuilder {
    return \Civi\Test::headless()
      ->install('osdi-client')
      ->installMe(__DIR__)
      ->apply();
  }

  public static function setUpBeforeClass(): void {
    static::$system = \OsdiClient\ActionNetwork\TestUtils::createRemoteSystem();
    \Civi\TceOsdiTestUtil::tellTheContainerToUseTCEClasses();
    static::setLocalTimeZone('CDT');
  }

  public function testBatchSyncFromCivi() {
    // set up the group that donors must belong to to be synced
    $testGroupName = 'ActionNetworkTest';
    $this->createGroup($testGroupName);

    // create a contact in the group, and their remote counterpart
    $personPairInGroup = $this->createInSyncPerson();
    $contactIdInGroup = $personPairInGroup->getLocalObject()->getId();
    $this->createGroupContact($contactIdInGroup, $testGroupName);

    // create a contact NOT in the group, and their remote counterpart
    $personPairOutside = $this->createInSyncPerson();
    $contactIdOutside = $personPairOutside->getLocalObject()->getId();

    // create donations in Civi
    $yesterday = date('Y-m-d H:i:s', time() - 60 * 60 * 24 * 1);

    $insiderContributionDetails = [
      ['total_amount' => 1.23, 'receive_date' => $yesterday],
      ['trxn_id' => 'testtrxn_insider'],
      $contactIdInGroup,
    ];
    $outsiderContributionDetails = [
      ['total_amount' => 4.56, 'receive_date' => $yesterday],
      ['trxn_id' => 'testtrxn_outsider'],
      $contactIdOutside,
    ];

    $insiderContributionId = $this->createLocalContribution(...$insiderContributionDetails);
    $outsiderContributionId = $this->createLocalContribution(...$outsiderContributionDetails);

    // Call system under test
    $batchSyncer = $this->getBatchSyncer();
    $batchSyncer->batchSyncFromLocal();

    // Check the in-group person, whose donation should have been synced
    $insiderLocalDonation = OsdiClient::container()->make(
      'LocalObject', 'Donation', $insiderContributionId);

    $insiderRemoteDonations = $personPairInGroup->getRemoteObject()->getDonations();

    self::assertEquals(1, $insiderRemoteDonations->rawCurrentCount());

    $this->assertCorrectlyMapped(
      $insiderLocalDonation->loadOnce(),
      $insiderRemoteDonations->rawFirst()->loadOnce()
    );

    $insiderSyncStates = \Civi\Api4\OsdiDonationSyncState::get(FALSE)
      ->addWhere('contribution_id', '=', $insiderContributionId)
      ->addWhere('remote_donation_id', '=', $insiderRemoteDonations->rawFirst()->getId())
      ->execute();

    self::assertCount(1, $insiderSyncStates);

    // Check the out-of-group person, whose donation should NOT have been synced
    $outsiderRemoteDonations = $personPairOutside->getRemoteObject()->getDonations();

    self::assertEquals(0, $outsiderRemoteDonations->rawCurrentCount());

    $outsiderSyncStates = \Civi\Api4\OsdiDonationSyncState::get(FALSE)
      ->addWhere('contribution_id', '=', $outsiderContributionId)
      ->execute();

    self::assertCount(0, $outsiderSyncStates);

  }

  protected function assertCorrectlyMapped($local, $remote): void {
    /** @var \Civi\Osdi\LocalObject\DonationTce $local */
    /** @var \Civi\Osdi\ActionNetwork\Object\Donation $remote */
    self::assertEquals($local->amount->get(), $remote->amount->get());

    $localDate = $local->receiveDate->get();
    $remoteDate = $remote->createdDate->get();
    $localZone = \Civi::settings()->get('osdiClient.localUtcOffset');

    self::assertEquals(
      strtotime("$localDate $localZone"),
      strtotime($remoteDate),
      "local $localDate vs remote $remoteDate");

    $syncState = OsdiPersonSyncState::get(FALSE)
      ->addWhere('contact_id', '=', $local->getPerson()->getId())
      ->addWhere('remote_person_id', '=', $remote->getDonor()->getId())
      ->execute();

    self::assertCount(1, $syncState);
  }

  protected function createGroup(string $testGroupName): void {
    \Civi\Api4\Group::save()
      ->addRecord([
        'name' => $testGroupName,
        'title' => $testGroupName,
        'is_hidden' => FALSE,
        'is_reserved' => FALSE,
      ])
      ->setMatch(['name'])
      ->execute();
  }

  /**
   * @param int|string $contactIdInGroup
   * @param string $testGroupName
   */
  protected function createGroupContact($contactIdInGroup, string $testGroupName): void {
    \Civi\Api4\GroupContact::save()
      ->addRecord([
        'contact_id' => $contactIdInGroup,
        'group_id:name' => $testGroupName,
        'status' => 'Added',
      ])
      ->setMatch(['group_id', 'contact_id'])
      ->execute();
  }

  protected function createLocalContribution(
    array $orderParams,
    array $paymentParams,
    int $contactId
  ): int {
    $orderParams += [
      'receive_date' => date('Y-m-d H:i:s', strtotime('now - 1 day')),
      'financial_type_id' => 1,
      'contact_id' => $contactId,
      'total_amount' => 1.23,
    ];
    $contribution = civicrm_api3('Order', 'create', $orderParams);

    $paymentParams += [
      'contribution_id' => $contribution['id'],
      'total_amount' => $orderParams['total_amount'],
      'trxn_date' => $orderParams['receive_date'],
      'trxn_id' => 'abc',
    ];
    civicrm_api3('Payment', 'create', $paymentParams);

    return $contribution['id'];
  }

  protected function getBatchSyncer(): \Civi\Osdi\ActionNetwork\BatchSyncer\DonationLimitedToGroup {
    $container = OsdiClient::container();
    $container->register('BatchSyncer', 'Donation', DonationLimitedToGroup::class);
    return $container->make('BatchSyncer', 'Donation');
  }

}
