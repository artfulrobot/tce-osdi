<?php

return [
  [
    'name' => 'Group_ActionNetworkTest',
    'entity' => 'Group',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'ActionNetworkTest',
        'title' => 'Action Network Test',
        'description' => 'Contacts to be synced from Civi to Action Network',
        'source' => NULL,
        'saved_search_id' => NULL,
        'is_active' => TRUE,
        'visibility' => 'User and User Admin Only',
        'where_clause' => NULL,
        'select_tables' => NULL,
        'where_tables' => NULL,
        'group_type' => NULL,
        'cache_date' => NULL,
        'refresh_date' => NULL,
        'parents' => NULL,
        'children' => NULL,
        'is_hidden' => FALSE,
        'is_reserved' => FALSE,
        'frontend_title' => NULL,
        'frontend_description' => NULL,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
];
