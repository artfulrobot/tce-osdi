<?php

return [
  [
    'name' => 'FinancialType_Texas_Campaign_for_the_Environment',
    'entity' => 'FinancialType',
    'cleanup' => 'unused',
    'update' => 'unmodified',
    'params' => [
      'version' => 4,
      'values' => [
        'name' => 'Texas Campaign for the Environment',
        'description' => 'Donations made on Action Network',
        'is_deductible' => FALSE,
        'is_reserved' => FALSE,
        'is_active' => TRUE,
      ],
      'match' => [
        'name',
      ],
    ],
  ],
];
