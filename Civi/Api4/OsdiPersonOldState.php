<?php
namespace Civi\Api4;

/**
 * OsdiPersonOldState entity.
 *
 * Provided by the TCE OSDI client extension.
 *
 * @package Civi\Api4
 */
class OsdiPersonOldState extends Generic\DAOEntity {

}
