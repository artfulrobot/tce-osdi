<?php

namespace Civi\Osdi\ActionNetwork\SingleSyncer;

use Civi\Osdi\LocalRemotePair;
use Civi\Osdi\PersonSyncState;

class PersonTce extends PersonBasic {

protected function savePairToSyncState(LocalRemotePair $pairAfter): PersonSyncState {
  /** @var \Civi\Osdi\ActionNetwork\Object\Person $remotePersonAfter */
  $remotePersonAfter = $pairAfter->getRemoteObject();
  if ($remotePersonAfter) {
    $remotePersonId = $pairAfter->getRemoteObject()->getId();
    if ($remotePersonId) {
      $phoneStatus = $remotePersonAfter->phoneStatus->get();
      $isSubscribedToSms = empty($phoneStatus) ? NULL : ('subscribed' === $phoneStatus);
      \Civi\Api4\OsdiPersonOldState::replace(FALSE)
        ->addWhere('remote_person_id', '=', $remotePersonId)
        ->setMatch(['remote_person_id'])
        ->setRecords([[
          'remote_person_id' => $remotePersonId,
          'sms_subscribed' => $isSubscribedToSms,
        ]])
        ->execute();
    }
  }
  return parent::savePairToSyncState($pairAfter);
}

}
