<?php

namespace Civi\Osdi\ActionNetwork\SingleSyncer;

use Civi\Osdi\LocalRemotePair;
use Civi\Osdi\Result\SyncEligibility;

class DonationTce extends DonationBasic {

  protected function getSyncEligibility(LocalRemotePair $pair): SyncEligibility {
    $result = parent::getSyncEligibility($pair);
    if (!$result->isStatus($result::ELIGIBLE)) {
      // We won't make any ineligible/error pairs eligible
      return $result;
    }

    if ($pair->isOriginLocal()) {
      /** @var \Civi\Osdi\LocalObject\DonationTce $localDonation */
      $localDonation = $pair->getLocalObject();
      $code = (array) $localDonation->tceFundraisingCode->get();
      if (array_intersect(['R Bad', 'R Refund'], $code)) {
        $result->setStatusCode($result::INELIGIBLE);
        $result->setMessage('Ineligible due to Fundraising Code');
        $result->setContext(['Fundraising Code' => $code]);
      }
    }

    return $result;
  }

}
