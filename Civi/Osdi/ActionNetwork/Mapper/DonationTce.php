<?php
namespace Civi\Osdi\ActionNetwork\Mapper;

use Civi\Osdi\ActionNetwork\Object\Donation as RemoteDonation;
use Civi\Osdi\Exception\CannotMapException;
use Civi\Osdi\LocalObjectInterface;
use Civi\Osdi\MapperInterface;
use Civi\Osdi\RemoteObjectInterface;
use Civi\OsdiClient;

class DonationTce extends DonationBasic implements MapperInterface {

  protected function mapRemoteFinancialTypeToLocal(RemoteDonation $remoteDonation): int {
    $financialTypeId =& \Civi::$statics['osdiClient.tceFinancialType'];
    if (empty($financialTypeId)) {
      try {
        $financialTypeId = \Civi\Api4\FinancialType::get(FALSE)
          ->addWhere('name', '=', 'TCE')
          ->execute()->single()['id'];
      }
      catch (\Throwable $exception) {
        throw new CannotMapException('Cannot sync a donation without the TCE financial type set up. %s: %s at %s:%s',
          get_class($exception), $exception->getMessage(), $exception->getFile(), $exception->getLine());
      }
    }
    return $financialTypeId;
  }

  /**
   * Override the default functionality to store the fundraising page title in a custom field.
   */
  protected function mapRemoteFundraisingPageToLocal(
    RemoteObjectInterface $remoteDonation,
    LocalObjectInterface $localDonation
  ): void {
    /** @var \Civi\Osdi\ActionNetwork\Object\Donation $remoteDonation */
    $pageTitle = $remoteDonation->getFundraisingPage()->title->get();
    $localDonation->source->set("Action Network: $pageTitle");
    $localDonation->tceFundraisingPage->set($pageTitle);
  }

  public function mapLocalToRemote(
    LocalObjectInterface $localDonation,
    RemoteObjectInterface $remoteDonation = NULL
  ): RemoteObjectInterface {
    /** @var \Civi\Osdi\ActionNetwork\Object\Donation $remoteDonation */
    $remoteDonation = parent::mapLocalToRemote($localDonation, $remoteDonation);

    if (!$remoteDonation->recurrence->get()['recurring']) {
      /** @var \Civi\Osdi\LocalObject\DonationTce $localDonation */
      $codes = $localDonation->tceFundraisingCode->get() ?? [];
      $map = [
        'FC' => 'None',
        'FC Recurring' => 'Monthly',
        'PC' => 'None',
        'PC Monthly' => 'Monthly',
        'PC Quarterly' => 'Every 3 Months',
        'PC Semi-annual' => 'None',
        'OL' => 'None',
        'OL Email' => 'None',
        'OL Event' => 'None',
        'OL Field canvass' => 'None',
        'DM' => 'None',
        'DM Annual report' => 'None',
        'DM End of year' => 'None',
        'PF' => 'None',
        'PF Recurring' => 'Monthly',
        'PF Austin' => 'None',
        'PF Austin recurring' => 'Monthly',
        'PF Houston' => 'None',
        'PF Houston recurring' => 'Monthly',
        'PF Dallas' => 'None',
        'PF Dallas recurring' => 'Monthly',
        'PF Anniversary' => 'None',
        'PF Event Austin' => 'None',
        'PF Event Houston' => 'None',
        'PF Event Dallas' => 'None',
        'PF Program fee' => 'None',
        'PF Earthshare' => 'None',
        'PF Tabling event' => 'None',
        'PF Dev' => 'None',
        'BF' => 'None',
        'BF Event' => 'None',
        'R Bad' => 'DO NOT IMPORT',
        'R Refund' => 'DO NOT IMPORT',
      ];
      $mappedCodes = array_intersect_key($map, array_flip($codes));
      $validPeriods = ['Weekly', 'Monthly', 'Every 3 Months', 'Yearly'];
      $chosenPeriods = array_intersect($validPeriods, $mappedCodes);
      foreach ($chosenPeriods as $period) {
        $remoteDonation->recurrence->set(['recurring' => TRUE, 'period' => $period]);
        break;
      }
    }

    return $remoteDonation;
  }

  /**
   * We map financial types to dummy fundraising pages
   */
  protected function mapLocalFundraisingPageToRemote(string $title, LocalObjectInterface $localDonation, RemoteDonation $remoteDonation): void {
    /** @var \Civi\Osdi\LocalObject\DonationTce $localDonation */
    $title = $localDonation->financialTypeLabel->get() . ' CiviCRM';
    if (substr($title, 0, 4) !== 'TCE ') {
      $title = "TCE $title";
    }
    try {
      parent::mapLocalFundraisingPageToRemote($title, $localDonation, $remoteDonation);
    }
    catch (CannotMapException $exception) {
      /** @var \Civi\Osdi\ActionNetwork\Object\FundraisingPage $page */
      $page = OsdiClient::container()->make('OsdiObject', 'osdi:fundraising_pages');
      $page->name->set($title);
      $page->title->set($title);
      $page->originSystem->set('CiviCRM');
      $page->save();
      $this->addRemoteFundraisingPageToCache($page);
      $remoteDonation->setFundraisingPage($page);
    }
  }

}
