<?php

namespace Civi\Osdi\ActionNetwork\Mapper;

use Civi\Api4\OsdiPersonOldState;
use Civi\Osdi\ActionNetwork\Object\Person as RemotePerson;
use Civi\Osdi\LocalObject\PersonBasic as LocalPerson;
use Civi\Osdi\LocalObjectInterface;
use Civi\Osdi\MapperInterface;
use Civi\Osdi\RemoteObjectInterface;
use Civi\OsdiClient;

class PersonTce extends PersonBasic implements MapperInterface {

  public function mapLocalToRemote(LocalObjectInterface $localPerson,
      RemoteObjectInterface $remotePerson = NULL): RemotePerson {

    /** @var \Civi\Osdi\LocalObject\PersonTce $l */
    /** @var \Civi\Osdi\ActionNetwork\Object\Person $remotePerson */

    $l = $localPerson->loadOnce();
    $remotePerson = $remotePerson ??
      OsdiClient::container()->make('OsdiObject', 'osdi:people');

    $remotePerson->givenName->set($l->firstName->get());
    $remotePerson->familyName->set($l->lastName->get());
    $remotePerson->languageSpoken->set(
      substr($l->preferredLanguage->get() ?? '', 0, 2));

    $noEmails = $l->isOptOut->get() || $l->doNotEmail->get() || ($l->emailOnHold->get() !== 'No');
    $remotePerson->emailAddress->set($l->emailEmail->get());
    $remotePerson->emailStatus->set($noEmails ? 'unsubscribed' : 'subscribed');

    $phoneNumber = $l->mobilePhoneNumber->get() ?: $l->nonMobilePhoneNumber->get();
    $smsOptIn = in_array('SMS', $l->preferredCommunicationMethod->get() ?? []);
    $noSms = !$smsOptIn || $l->isOptOut->get() || $l->doNotSms->get() || empty($phoneNumber);
    $remotePerson->phoneNumber->set($phoneNumber ?? '');
    $remotePerson->phoneStatus->set($noSms ? 'unsubscribed' : 'subscribed');

    $this->mapLocalToRemoteAddress($l, $remotePerson);

    $remoteCustomFields = $remotePerson->customFields->get() ?? [];
    $remoteCustomFields['CiviCRM Contact ID'] = $l->getId();
    if (empty($remotePerson->getId())) {
      $remoteCustomFields['Contact Source'] = 'CiviCRM';
    }
    $remotePerson->customFields->set($remoteCustomFields);

    return $remotePerson;
  }

  public function mapRemoteToLocal(RemoteObjectInterface $remotePerson,
      LocalObjectInterface $localPerson = NULL): LocalPerson {

    /** @var \Civi\Osdi\LocalObject\PersonTce $localPerson */
    /** @var \Civi\Osdi\ActionNetwork\Object\Person $remotePerson */

    $localPerson = $localPerson ??
      OsdiClient::container()->make('LocalObject', 'Person');

    $this->setWithNonEmptyValue($localPerson->firstName, $remotePerson->givenName->get());
    $this->setWithNonEmptyValue($localPerson->lastName, $remotePerson->familyName->get());
    if ($mappedLanguage = $this->mapLanguageFromActionNetwork($remotePerson)) {
      $localPerson->preferredLanguageName->set($mappedLanguage);
    }

    if ($rpEmail = $remotePerson->emailAddress->get()) {
      $lpEmail = $localPerson->emailEmail->get() ?? '';
      if (strtolower($rpEmail) !== strtolower($lpEmail)) {
        $localPerson->emailEmail->set($rpEmail);
      }
    }

    if ($rpPhone = $remotePerson->phoneNumber->get()) {
      $localPerson->mobilePhoneNumber->set($rpPhone);
      $pref = (array) $localPerson->preferredCommunicationMethod->get();
      if ('subscribed' === $remotePerson->phoneStatus->get()) {
        $pref = array_unique(array_merge($pref, ['SMS']));
        $localPerson->preferredCommunicationMethod->set($pref);
        $localPerson->doNotSms->set(FALSE);
      }
      else {
        // remote person is currently NOT subscribed to SMS; were they previously?
        $rpId = $remotePerson->getId();
        if ($rpId) {
          $previouslySubscribedToSms = OsdiPersonOldState::get(FALSE)
            ->addWhere('remote_person_id', '=', $rpId)
            ->execute()->last()['sms_subscribed'] ?? NULL;
          if ($previouslySubscribedToSms) {
            // they have just unsubscribed
            $pref = array_diff($pref, ['SMS']);
            $localPerson->preferredCommunicationMethod->set($pref);
            $localPerson->doNotSms->set(TRUE);
          }
        }
      }
    }

    $this->mapRemoteToLocalAddress($remotePerson, $localPerson);

    $localPersonIsNew = empty($localPerson->getId());
    if ($localPersonIsNew) {
      $localPerson->source->set('Action Network');
    }

    if ('subscribed' == $remotePerson->emailStatus->get()) {
      $localPerson->doNotEmail->set(FALSE);
      $localPerson->isOptOut->set(FALSE);
      $localPerson->emailOnHold->set('No');
      $localPerson->dontEmailReason->set(NULL);
    }
    else {
      $alreadyOptedOut = $localPerson->doNotEmail->getAsLoaded() || $localPerson->isOptOut->getAsLoaded();
      if (!$localPersonIsNew && !$alreadyOptedOut) {
        $localPerson->dontEmailReason->set('Action Network Unsubscribe');
      }
      $localPerson->doNotEmail->set(TRUE);
      $localPerson->isOptOut->set(TRUE);
    }

    return $localPerson;
  }

  /**
   * @param \Civi\Osdi\LocalObject\Field|\Civi\Osdi\ActionNetwork\Object\Field $field
   * @param mixed $value
   *
   * @return void
   */
  protected function setWithNonEmptyValue($field, $value) {
    if (!empty($value)) {
      $field->set($value);
    }
  }

}
