<?php

namespace Civi\Osdi\ActionNetwork\BatchSyncer;

use Civi\Api4\Contribution;

class DonationTce extends DonationBasic {

  protected function findNewLocalDonations(string $cutoff): \Civi\Api4\Generic\Result {
    return Contribution::get(FALSE)
      ->addWhere('contribution_status_id:name', '=', 'Completed')
      ->addJoin(
        'OsdiDonationSyncState AS donation_sync_state',
        'EXCLUDE',
        ['id', '=', 'donation_sync_state.contribution_id']
      )
      ->execute();
  }

}
