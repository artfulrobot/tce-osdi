<?php

namespace Civi\Osdi\ActionNetwork\BatchSyncer;

class PersonLimitedToGroup extends PersonBasic {

  protected function getCandidateLocalContacts($cutoff): \Civi\Api4\Generic\Result {
    $civiContacts = \Civi\Api4\Email::get(FALSE)
      ->addSelect(
        'contact_id',
        'contact.modified_date',
        'sync_state.local_pre_sync_modified_time',
        'sync_state.local_post_sync_modified_time')
      ->addJoin('Contact AS contact', 'INNER')
      ->addJoin(
        'OsdiPersonSyncState AS sync_state',
        'LEFT',
        ['contact_id', '=', 'sync_state.contact_id'])
      ->addJoin('GroupContact AS group_contact', 'INNER',
        ['contact_id', '=', 'group_contact.contact_id'])
      ->addJoin('Group AS group', 'INNER')
      ->addWhere('group.name', '=', 'ActionNetworkTest')
      ->addWhere('group_contact.status', '=', 'Added')
      ->addClause('OR',
        ['contact.modified_date', '>=', $cutoff],
        ['sync_state.id', 'IS NULL'])
      ->addWhere('is_primary', '=', TRUE)
      ->addWhere('contact.is_deleted', '=', FALSE)
      ->addWhere('contact.is_opt_out', '=', FALSE)
      ->addWhere('contact.contact_type', '=', 'Individual')
      ->addGroupBy('email')
      ->addOrderBy('contact.modified_date')
      ->execute();
    return $civiContacts;
  }

}
