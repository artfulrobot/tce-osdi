<?php

namespace Civi\Osdi\ActionNetwork\BatchSyncer;

use Civi\Api4\Contribution;

class DonationLimitedToGroup extends DonationBasic {

  protected function findNewLocalDonations(string $cutoff): \Civi\Api4\Generic\Result {
    return Contribution::get(FALSE)
      //->addWhere('receive_date', '>=', $cutoff)
      ->addWhere('contribution_status_id:name', '=', 'Completed')
      ->addJoin(
        'OsdiDonationSyncState AS donation_sync_state',
        'EXCLUDE',
        ['id', '=', 'donation_sync_state.contribution_id']
      )
      ->addJoin('GroupContact AS group_contact', 'INNER',
        ['contact_id', '=', 'group_contact.contact_id'])
      ->addJoin('Group AS group', 'INNER')
      ->addWhere('group.name', '=', 'ActionNetworkTest')
      ->addWhere('group_contact.status', '=', 'Added')
      ->execute();
  }

}
