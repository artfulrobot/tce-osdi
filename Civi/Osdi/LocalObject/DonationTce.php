<?php

namespace Civi\Osdi\LocalObject;

use Civi\Osdi\LocalObjectInterface;

class DonationTce extends DonationBasic implements LocalObjectInterface {

  public Field $tceFundraisingPage;
  public Field $tceFundraisingCode;

  protected static function getFieldMetadata(): array {
    return parent::getFieldMetadata() + [
      'tceFundraisingPage' => ['select' => 'Record_Source.an_fundraising_page'],
      'tceFundraisingCode' => ['select' => 'Field_Canvass.Field_Canvass_Respons'],
    ];
  }

  protected function getOrderCreateParamsForSave(): array {
    static $extraFieldKeys = [];

    if (empty($extraFieldKeys)) {
      $fields = static::getFieldMetadata();
      foreach (['tceFundraisingPage', 'tceFundraisingCode'] as $camelName) {
        [$group, $name] = explode('.', $fields[$camelName]['select']);
        $id = \CRM_Core_BAO_CustomField::getCustomFieldID($name, $group);
        $extraFieldKeys[$camelName] = "custom_$id";
      }
    }

    $orderParams = parent::getOrderCreateParamsForSave();

    $codes = (array) $this->tceFundraisingCode->get();
    if ($codes) {
      $formattedCodes = array_fill_keys($codes, 1);
      $orderParams[$extraFieldKeys['tceFundraisingCode']] = $formattedCodes;
    }


    $fundraisingPage = $this->tceFundraisingPage->get();
    if ($fundraisingPage) {
      $orderParams[$extraFieldKeys['tceFundraisingPage']] = $fundraisingPage;
    }

    return $orderParams;
  }

}
