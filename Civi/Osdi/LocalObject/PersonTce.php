<?php

namespace Civi\Osdi\LocalObject;

use Civi\Api4\Phone;

class PersonTce extends PersonBasic {

  public Field $dontEmailReason;

  public Field $emailOnHold;

  public Field $mobilePhoneId;

  public Field $mobilePhoneNumber;

  public Field $mobilePhoneNumberNumeric;

  public Field $nonMobilePhoneId;

  public Field $nonMobilePhoneNumber;

  public Field $nonMobilePhoneNumberNumeric;

  public Field $source;

  public Field $preferredCommunicationMethod;

  /**
   * @var array|null
   * Use the parent's methods, but store our own metadata
   */
  protected static ?array $fieldMetadata = NULL;

  public static function getJoins(): array {
    return [
      ['Email AS email', 'LEFT', NULL, ['email.is_primary', '=', 1]],
      ['Phone AS mobile_phone', 'LEFT', NULL,
        ['mobile_phone.phone_type_id:name', '=', '"Mobile"'],
      ],
      ['Phone AS non_mobile_phone', 'LEFT', NULL,
        ['non_mobile_phone.phone_type_id:name', '<>', '"Mobile"'],
      ],
      ['Address AS address', FALSE, NULL,
        ['address.is_primary', '=', 1],
      ],
    ];
  }

  public static function getOrderBys(): array {
    return [
      'mobile_phone.is_primary' => 'DESC',
      'mobile_phone.id' => 'ASC',
      'non_mobile_phone.is_primary' => 'DESC',
      'non_mobile_phone.id' => 'ASC',
    ];
  }

  protected static function getContactFieldMetadata(): array {
    return parent::getContactFieldMetadata() +
      [
        'source' => ['select' => 'source'],
        'dontEmailReason' => ['select' => 'Communication_Details.Reason_for_Do_Not_Email'],
        'preferredCommunicationMethod' => ['select' => 'preferred_communication_method:name'],
      ];
  }

  protected static function getEmailFieldMetadata(): array {
    return parent::getEmailFieldMetadata() +
      ['emailOnHold' => ['select' => 'email.on_hold:label']];
  }

  protected static function getPhoneFieldMetadata(): array {
    return [
      'nonMobilePhoneId' => ['select' => 'non_mobile_phone.id'],
      'nonMobilePhoneNumber' => ['select' => 'non_mobile_phone.phone'],
      'nonMobilePhoneNumberNumeric' => [
        'select' => 'non_mobile_phone.phone_numeric',
        'readOnly' => TRUE,
      ],
      'mobilePhoneId' => ['select' => 'mobile_phone.id'],
      'mobilePhoneNumber' => ['select' => 'mobile_phone.phone'],
      'mobilePhoneNumberNumeric' => [
        'select' => 'mobile_phone.phone_numeric',
        'readOnly' => TRUE,
      ],
    ];
  }

  protected function getContactRecordForSave(): array {
    return parent::getContactRecordForSave()
      + $this->getSaveableFieldContents('Communication_Details', TRUE);
  }

  protected function savePhone($cid): void {
    $this->saveOrDeleteMobilePhone($cid);
    $this->saveNonMobilePhone($cid);
  }

  protected function saveOrDeleteMobilePhone($cid): void {
    $phoneId = $this->mobilePhoneId->get();
    $phone = $this->mobilePhoneNumber->get();
    if (empty($phone)) {
      if (!empty($phoneId)) {
        Phone::delete(FALSE)
          ->addWhere('id', '=', $phoneId)
          ->execute();
      }
      return;
    }

    $this->savePhoneFinalStep($phoneId, $cid, $phone, 'Mobile');
  }

  protected function saveNonMobilePhone($cid): void {
    if (empty($phone = $this->nonMobilePhoneNumber->get())) {
      return;
    }
    $phoneId = $this->nonMobilePhoneId->get();
    $this->savePhoneFinalStep($phoneId, $cid, $phone, 'Phone');
  }

  private function savePhoneFinalStep($phoneId, $cid, $phone, $phoneType): void {
    Phone::save(FALSE)
      ->setMatch([
        'contact_id',
        'phone',
      ])
      ->addRecord([
        'id' => $phoneId,
        'contact_id' => $cid,
        'phone' => $phone,
        'phone_type_id:name' => $phoneType,
      ])->execute();
  }

}
