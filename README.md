# tce-osdi

This CiviCRM extension builds on [OSDI-Client](https://github.com/lemniscus/osdi-client/) to provide custom sync extension for TCE.

The extension is licensed under [AGPL-3.0](LICENSE.txt).

## Requirements

* PHP v7.4+
* CiviCRM 5.49

## Customisations

This extension adds a custom field group to Contributions, *Record Source*,
if it does not already exist. It adds one field to this group:
*Action Network Fundraising Page*, which is used to record imported Action
Network Donations' fundraising page titles.

